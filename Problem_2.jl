### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ a7362460-b441-11ec-0df4-93ad19666239
using Flux, Pkg ,Markdown ,Images,Plots,Random,Printf,ImageSegmentation

# ╔═╡ 95a7e321-415f-4928-ac55-471bdc1937ed
Pkg.add("MLDataUtils")

# ╔═╡ bd9ab4ec-b020-4716-91c0-9b1ac76cdf61
Pkg.add("DataFrames")

# ╔═╡ a4892312-0e0c-4644-8835-f4033da61df8
using MLDataUtils, InteractiveUtils

# ╔═╡ b239912d-db91-4868-acc4-c868f652924f
using DataFrames

# ╔═╡ 301d38d4-5782-417c-b2d5-9525e82f1d1c
using Base.Iterators: partition, repeated

# ╔═╡ 31c248b0-2144-4c03-bbae-9ddcf0a11216
using Flux: onehotbatch, crossentropy, onecold, @epochs

# ╔═╡ 184f631b-7573-4623-8b22-fe67e07d37ec
md"# AIG Assignment Problem 2"

# ╔═╡ d967d094-d40e-4bfe-9736-3aaba1299ba2
pixel = 300

# ╔═╡ 2336c79f-f68b-40ac-874f-5242a6ee3628
function holdOut(Patterns::Int, percentageTest::Float64)
    @assert ((percentageTest>=0.) & (percentageTest<=1.));
    indices = randperm(Patterns);
    trainingPatterns = Int(round(Patterns*(1-percentageTest)));
    return (indices[1:trainingPatterns], indices[trainingPatterns+1:end]);
end

# ╔═╡ bb1c3f39-04e6-4d0d-8670-7f440bc2b652
function prepare_data(dir, h_size, w_size, label)
    directory= readdir(dir);
    pattern = Array{Array{RGB{Normed{UInt8,8}},2}}(undef, 0)
    labelS = zeros(size(directory,1))
    i=1;
    for file in directory
        img = load(string(dir,"/",file));
        
        imgR = imresize(img, (h_size, w_size));
        push!(pattern, imgR);
        labelS[i] = label;
        i = i + 1;
    end
    return (pattern, labelS);
end

# ╔═╡ 40802452-3ac4-42dd-b511-22e82061992c
function get_patterns_divided(pat, res, gTest)
    x_train = Array{Array{Float32,3}}(undef, 0)
    test = Array{Array{Float32,3}}(undef, 0)
    
    numP = size(pat,1);
    
    (lEntre, lTest) = holdOut(numP, gTest)

    y_train = Array{Int64,1}(undef, 0)
    rt =Array{Int64,1}(undef, 0)
    
    for i = 1:size(lEntre,1)
        push!(x_train, permutedims(channelview(pat[lEntre[i]]),[2,3,1]))
        push!(y_train, res[lEntre[i]])
    end
    for i = 1:size(lTest,1)
        push!(test, permutedims(channelview(pat[lTest[i]]),[2,3,1]))
        push!(rt, res[lTest[i]])
    end
    
    return (x_train,y_train,lEntre),(test,rt,lTest);
end

# ╔═╡ ab1f3b4c-0a9b-4871-8a41-3a949f222056
function load_data(path)
	x = readdir(path, join = true)
	return x
end

# ╔═╡ 68c17efb-f50b-47f1-a873-ddf460643f62
train_normal = load_data("C:\\Users\\Personal Computer Rx\\Desktop\\NUST\\2022\\AIG\\archive (1)\\chest_xray\\test\\NORMAL")

# ╔═╡ 7aabb0db-a621-4e65-99bb-cd1899715df6
train_pneumonia= load_data("C:\\Users\\Personal Computer Rx\\Desktop\\NUST\\2022\\AIG\\archive (1)\\chest_xray\\test\\PNEUMONIA")

# ╔═╡ 8a04c255-8bf6-400a-83df-bccb5d4093c8
function process_images(directory, width::Int64, height::Int64)
    files_list = readdir(directory)
   
end

# ╔═╡ 436b16f5-64a1-4faa-b856-26776f5d78c2
n_resolution = 90

# ╔═╡ 3e415e0f-e175-4641-a6bf-0e8cbcd971c9
begin
    process_images("C:\\Users\\Personal Computer Rx\\Desktop\\NUST\\2022\\AIG\\archive (1)\\chest_xray\\test\\PNEUMONIA", n_resolution, n_resolution)
   
    process_images("C:\\Users\\Personal Computer Rx\\Desktop\\NUST\\2022\\AIG\\archive (1)\\chest_xray\\test\\NORMAL", n_resolution, n_resolution)
    end

# ╔═╡ 8bcd46d9-9a38-456c-b169-545db4aab60b
begin
    NORMAL_dir = readdir("C:\\Users\\Personal Computer Rx\\Desktop\\NUST\\2022\\AIG\\archive (1)\\chest_xray\\test\\NORMAL")
     PNEUMONIA_dir = readdir("C:\\Users\\Personal Computer Rx\\Desktop\\NUST\\2022\\AIG\\archive (1)\\chest_xray\\test\\PNEUMONIA");
end;

# ╔═╡ c2a090e2-3c1f-4795-a4b5-82f7ee695048
begin
    
   NORMAL = load.("C:\\Users\\Personal Computer Rx\\Desktop\\NUST\\2022\\AIG\\archive (1)\\chest_xray\\test\\NORMAL\\" .*  NORMAL_dir)
    PNEUMONIA = load.("C:\\Users\\Personal Computer Rx\\Desktop\\NUST\\2022\\AIG\\archive (1)\\chest_xray\\test\\PNEUMONIA\\" .* PNEUMONIA_dir);
end;

# ╔═╡ 5d89fdf0-1c29-4953-9604-796777935b6f
function concat_3d(NORMAL_test, NORMAL_train,  PNEUMONIA_test, PNEUMONIA_train)
    p = vcat(NORMAL_test, NORMAL_train)
    r = vcat(PNEUMONIA_test,PNEUMONIA_train )
    return (p,r)
end

# ╔═╡ f6c0cd13-1c9d-4fa5-88cb-5992fb61e6ae
begin
    labels = vcat([0 for _ in 1:length(NORMAL)], [1 for _ in 1:length(PNEUMONIA)])
    (x_train, y_train), (x_test, y_test) = splitobs(shuffleobs((data, labels)), at = 0.7)
end;

# ╔═╡ 8ba6eb0c-0f3c-4040-a8d2-a42560e70062
function make_batch(X, Y, idxs)
    X_batch = Array{Float32}(undef, size(X[1])...,length(idxs))
    for i in 1:length(idxs)
        X_batch[:, :, :, i] = Float32.(X[idxs[i]])
    end
    Y_batch = onehotbatch(Y[idxs], 0:1)
    return (X_batch, Y_batch)
end

# ╔═╡ 7eab2f7b-e37c-4ff7-8259-57bf2ca93418
batch_size = 140

# ╔═╡ 0985989c-5efd-400a-bcb7-32155f796332
model = Chain(
    Conv((3, 3), 3=>16, pad=(1,1), relu),
    MaxPool((2,2)), 
    Conv((3, 3), 16=>32, pad=(1,1), relu),
    MaxPool((2,2)),
    Conv((3, 3), 32=>32,pad=(1,1), relu),
    MaxPool((2,2)),
    x -> reshape(x, :, size(x, 4)),
    Dense(20000,2),
    softmax,
);

# ╔═╡ 5a432576-2f28-43f5-81d8-5873fd3669ad
begin
    train_loss = Float64[]
    test_loss = Float64[]
    acc = Float64[]
    ps = Flux.params(model)
    opt = ADAM()
    L(x, y) = Flux.crossentropy(model(x), y)
    L((x,y)) = Flux.crossentropy(model(x), y)
    accuracy(x, y, f) = mean(Flux.onecold(f(x)) .== Flux.onecold(y))
	accuracy_target = 0.8
    
    function update_loss!()
        push!(train_loss, mean(L.(train_set)))
        push!(test_loss, mean(L(test_set)))
        push!(acc, accuracy(test_set..., model))
        @printf("train loss = %.2f, test loss = %.2f, accuracy = %.2f\n", train_loss[end], test_loss[end], acc[end])
    end
end

# ╔═╡ 857282bf-798e-4fc3-baff-501a437bfd1c
begin
	Flux.testmode!(model,false)   
	@epochs 20 Flux.train!(L, ps, train_set, opt;cb = Flux.throttle(update_loss!, 8))
	Flux.testmode!(model,true)   
end

# ╔═╡ Cell order:
# ╠═184f631b-7573-4623-8b22-fe67e07d37ec
# ╠═a7362460-b441-11ec-0df4-93ad19666239
# ╟─95a7e321-415f-4928-ac55-471bdc1937ed
# ╠═a4892312-0e0c-4644-8835-f4033da61df8
# ╠═bd9ab4ec-b020-4716-91c0-9b1ac76cdf61
# ╠═b239912d-db91-4868-acc4-c868f652924f
# ╠═301d38d4-5782-417c-b2d5-9525e82f1d1c
# ╠═31c248b0-2144-4c03-bbae-9ddcf0a11216
# ╠═d967d094-d40e-4bfe-9736-3aaba1299ba2
# ╠═2336c79f-f68b-40ac-874f-5242a6ee3628
# ╠═bb1c3f39-04e6-4d0d-8670-7f440bc2b652
# ╠═40802452-3ac4-42dd-b511-22e82061992c
# ╠═ab1f3b4c-0a9b-4871-8a41-3a949f222056
# ╠═68c17efb-f50b-47f1-a873-ddf460643f62
# ╠═7aabb0db-a621-4e65-99bb-cd1899715df6
# ╠═8a04c255-8bf6-400a-83df-bccb5d4093c8
# ╠═436b16f5-64a1-4faa-b856-26776f5d78c2
# ╠═3e415e0f-e175-4641-a6bf-0e8cbcd971c9
# ╠═8bcd46d9-9a38-456c-b169-545db4aab60b
# ╠═c2a090e2-3c1f-4795-a4b5-82f7ee695048
# ╠═5d89fdf0-1c29-4953-9604-796777935b6f
# ╠═f6c0cd13-1c9d-4fa5-88cb-5992fb61e6ae
# ╠═8ba6eb0c-0f3c-4040-a8d2-a42560e70062
# ╠═7eab2f7b-e37c-4ff7-8259-57bf2ca93418
# ╠═0985989c-5efd-400a-bcb7-32155f796332
# ╠═5a432576-2f28-43f5-81d8-5873fd3669ad
# ╠═857282bf-798e-4fc3-baff-501a437bfd1c
